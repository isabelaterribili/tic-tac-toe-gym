import React, { Component } from 'react';
import { View, Button, StyleSheet } from 'react-native';
import { MaterialCommunityIcons as Icon } from 'react-native-vector-icons';

export default class IconTicaTacToe extends React.Component {   

  constructor(props) {
    super(props);
    }

  renderIcon = (row, column) => {
    var currentGame = this.props.gameState[row][column];
    
    switch(currentGame) {
      case 0: return <View />
      case 1: return <Icon name='close' style={styles.x} />
      case -1: return <Icon name='circle-outline' style={styles.o} />
    }
  }
  render() {
    return(
      <View>{this.renderIcon(0, 0)}</View>
    );
  }
}

const styles = StyleSheet.create({
  x: {
    color: '#FFF209',
    fontSize: 60,
  }, 
  o: {
    color: '#FFFFF8',
    fontSize: 60,
  },
}); 


  