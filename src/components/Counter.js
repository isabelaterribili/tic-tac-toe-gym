import React, { Component } from 'react';
import { Button, Text } from 'react-native';
import { MaterialCommunityIcons as Icon } from 'react-native-vector-icons';


export default class Counter extends React.Component {   
    constructor(props) {
        super(props);
        this.state = {
            currentRound: 0,
        }
    }

renderCounter = () => {
    <Text style={[styles.counter]}>{this.state.currentRound}</Text>
  }
  
  countRounds(currentRound) {
    var nextRound = (currentRound < 5) ? currentRound++ : this.showResumeGameMessage();
    this.setState({currentRound: nextRound})
  }
  render() {
    return(
        <View style={styles.counter}>{this.renderCounter}</View>
    );
  }
}

const styles = StyleSheet.create({
    counter: { 
        color: '#FFF209',   
        fontSize: 60, 
      },
})