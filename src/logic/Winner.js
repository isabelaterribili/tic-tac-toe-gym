import React, { Component } from 'react';
import { View, Button } from 'react-native';

const SUM_PLAYER_NUMBERS = 3;

export function findWinner(gameState) {
    var grid = gameState;
    var winnerSum;

    findOnRow(grid, winnerSum);

    findOnColumn(grid, winnerSum);
    
    findOnDiagonal(grid, winnerSum);

    findOnReverseDiagonal(grid, winnerSum);

    return 0;
}

export function findOnRow (grid, winnerSum) {
    for(var i = 0; i < SUM_PLAYER_NUMBERS; i++) {
        winnerSum = grid[i][0] + grid[i][1] + grid[i][2];
        if (winnerSum == 3) {return 1;}
        else if (winnerSum == -3) {return -1};
    }
}

export function findOnColumn(grid, winnerSum) {
    for(var i = 0; i < SUM_PLAYER_NUMBERS; i++) {
        winnerSum = grid[0][i] + grid[1][i] + grid[2][i];
        
        if (winnerSum == 3) {return 1;}
        else if (winnerSum == -3) {return -1};
    }
}

export function findOnDiagonal(grid, winnerSum) {
    winnerSum = grid[0][0] + grid[1][1] + grid[2][2];
    if (winnerSum == 3) {return 1;}
    else if (winnerSum == -3) {return -1};
}

export function findOnReverseDiagonal(grid, winnerSum) {
    winnerSum = grid[2][0] + grid[1][1] + grid[0][2];
    if (winnerSum == 3) {return 1;}
    else if (winnerSum == -3) {return -1};
}

export function showResumeGameMessage(winner) {
    if(winner == 0) {
        Alert.alert('There is no winner!')}
         else {
        Alert.alert('Congrats player ' + winner + '! You are a winner')}
}