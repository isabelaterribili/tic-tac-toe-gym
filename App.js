import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Button } from 'react-native';
  
import { MaterialCommunityIcons as Icon } from 'react-native-vector-icons';

import IconTicTacToe from './src/components/IconTicTacToe'
  
export default class App extends React.Component {   
  constructor(props) {
    super(props);
    this.state = {
      gameState: [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
      ],
      currentPlayer: 1,
      currentRound: 0,
    }
  }

  _onSquarePress(row, column) {      
    var pressedSquareValue = this.state.gameState[row][column];
    if(pressedSquareValue !== 0) { return; }
    
    var currentPlayer = this.state.currentPlayer;
    
    var grid = this.state.gameState.slice();
    grid[row][column] = currentPlayer;
    this.setState({gameState: grid});
    
    this.setCurrentPlayer(currentPlayer);
    this.countRounds(this.state.currentRound)
    
    this.showsWinner();
  }
  
  findWinner = () => {
    const SUM_PLAYER_NUMBERS = 3;
    var grid = this.state.gameState;
    var winnerSum;
    
    for(var i = 0; i < SUM_PLAYER_NUMBERS; i++) {
      winnerSum = grid[i][0] + grid[i][1] + grid[i][2];
      
      if (winnerSum == 3) {return 1;}
      else if (winnerSum == -3) {return -1};
    }
    
    for(var i = 0; i < SUM_PLAYER_NUMBERS; i++) {
      winnerSum = grid[0][i] + grid[1][i] + grid[2][i];
      
      if (winnerSum == 3) {return 1;}
      else if (winnerSum == -3) {return -1};
    }
    
    winnerSum = grid[0][0] + grid[1][1] + grid[2][2];
    (winnerSum == 3) ? -1 : (winnerSum == -3) ? -1 : winnerSum = grid[2][0] + grid[1][1] + grid[0][2];
  
    if (winnerSum == 3) {return 1;}
    else if (winnerSum == -3) {return -1};
        
    return 0;
  }

  showsWinner(winner) {
    var winner = this.findWinner();
    if (winner == 1 || winner == -1) {
      this.showResumeGameMessage(winner)
      this.restartGame();
    }
  } 

  showResumeGameMessage(winner) {
    if(winner == 0) {
      Alert.alert('There is no winner!')
    } else {
      Alert.alert('Congrats player ' + winner + ' ! You are a winner')
    }
  }

  restartGame() {
    this.setupGame();
  }

  setupGame = () => {
    this.setState({gameState:
      [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
      ], 
      currentPlayer: 1,
    });
  }

  setCurrentPlayer(currentPlayer) {
    var nextPlayer = (currentPlayer == 1) ? -1 : 1;
    this.setState({currentPlayer: nextPlayer})
  }

  renderIcon = (row, column) => {
    var currentGame = this.state.gameState[row][column];
    
    switch(currentGame) {
      case 0: return <View />
      case 1: return <Icon name='close' style={styles.x} />
      case -1: return <Icon name='circle-outline' style={styles.o} />
    }
  }
  
  renderCounter() {
    <Text style={[styles.counter]}>{this.state.currentRound}</Text>
  }
  
  countRounds(currentRound) {
    var nextRound = (currentRound < 5) ? currentRound++ : this.showResumeGameMessage();
    this.setState({currentRound: nextRound})
  }
  
  render() {
    return (
      <View style={[styles.container]}>
        <View style={[styles.counter]}>{this.renderCounter()}</View>
            <View style={styles.grid}>
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(0, 0)}>
              {this.renderIcon(0, 0)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(0, 1)}>
              {this.renderIcon(0, 1)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(0, 2)}>
              {this.renderIcon(0, 2)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(1, 0)}>
              {this.renderIcon(1, 0)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(1, 1)}>
              {this.renderIcon(1, 1)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(1, 2)}>
              {this.renderIcon(1, 2)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(2, 0)}>
              {this.renderIcon(2, 0)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(2, 1)}>
              {this.renderIcon(2, 1)}
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.square} onPress={() => this._onSquarePress(2, 2)}>
              {this.renderIcon(2, 2)}
            </TouchableOpacity>
            
            <View style={styles.restartGameButton}>
              <Button onPress={() => this.restartGame()}
              title="Restart" 
              color='#0C121D'/>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  grid: {
    flexDirection: 'row', 
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'center',
    top: 100,
  },
  square: {
    width: '30%', 
    margin: '1%', 
    aspectRatio: 1,
    backgroundColor: '#043C53',
    alignItems: 'center',
    justifyContent: 'center',
  },
  x: {
    color: '#FFF209',
    fontSize: 60,
  }, 
  o: {
    color: '#FFFFF8',
    fontSize: 60,
  },
  restartGameButton: {
    width: '95%', 
    margin: 10,
    height: 30,
  },
  counter: { 
    color: '#FFF209',   
    fontSize: 60, 
  },
}); 
